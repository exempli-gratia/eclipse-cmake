/** @mainpage eclipse-cmake - Reliable Embedded Systems
 *
 * @author Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
 * @version 1.0.0
**/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> /* for sleep */
#define DEBUG 1
/**
 * Main class of project eclipse-cmake
 *
 * @param argc the number of arguments
 * @param argv the arguments from the commandline
 * @returns exit code of the application
 */
int main (void) {
        int i;
        printf ("Hello World from cmake\n"); /* prints Hello World */

        for ( i =0; i <10; i ++) {
                printf ("cmake iteration %d\n" ,i);
        }

#if DEBUG > 0
        sleep(5) ; /* sleep to see output if we run it from Eclipse */
#endif
return 0;
}
