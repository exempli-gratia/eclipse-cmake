cmake_minimum_required (VERSION 3.20)

######## Project settings ########
PROJECT(eclipse-cmake)
SET(LICENSE "MIT")

######## Build and include settings ########
include_directories(
	inc
)

link_directories(
	${LINK_DIRECTORIES}
)


file(GLOB SOURCES
	"src/*.c"
)

add_executable(
	eclipse-cmake

	${SOURCES}
)

TARGET_LINK_LIBRARIES(
	eclipse-cmake
)

######## Install targets ########
INSTALL(TARGETS eclipse-cmake
	RUNTIME DESTINATION usr/bin
)
